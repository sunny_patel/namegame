import 'dart:async';
import 'dart:ui';
import 'package:audioplayer/audioplayer.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import '../config.dart';

class GameClock {

  AudioPlayer _audioPlayer;
  int totalGameTime = 1;
  bool killTimer = false;
  StreamSubscription _tickerStream;
  VoidCallback onGameOver; 
  Stream _stream;
  BehaviorSubject<double> gameClockStream;

  GameClock({
    @required
    totalGameTimeInSeconds,
    this.onGameOver
  }) {
    this.totalGameTime = totalGameTimeInSeconds;
    this._audioPlayer = new AudioPlayer();
    this.gameClockStream = new BehaviorSubject<double>(seedValue: 0.0);
    this._stream = new Stream<double>.periodic(
      Duration(milliseconds: 1),
      (tick) {
        return (tick * 1.0) / 1000;
      }
    ).take((this.totalGameTime + 1) * 1000)
    .takeWhile((tick) {
      return !killTimer;
    });
  }

  start() {
    this._tickerStream = this._stream.listen((tick) {
      if (tick > 0 && tick < (this.totalGameTime * (1/3))) {
        if (tick % 3 == 0) {
          playBeep();
        }
      } else if (tick >= (this.totalGameTime * (1/3)) && tick <= (this.totalGameTime * (2/3))) {
        if (tick % 2 == 0) {
          playBeep();
        }
      } else {
        playBeep();
      }

      this.gameClockStream.add(tick);
    });

    this._tickerStream.onDone(() {
      if (!this.killTimer) {
        this._audioPlayer.play(Config.buzzerSoundUrl);
      }
      this.onGameOver();
    });
  }

  playBeep() {
    this._audioPlayer.play(Config.beep3SoundUrl);
  }

  stop() {
    print('stopping timer');
    this.killTimer = true;
    this._tickerStream.cancel();
    this._audioPlayer.stop();
  }
}