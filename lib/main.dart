import 'package:flutter/material.dart';

import 'screens/landing/landing.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        primaryColor: Color(0xFF6200EE),
        primaryColorDark:Color(0xFF3700B3),
        accentColor: Color(0xFF03DAC5),
        backgroundColor: Colors.white,
        primaryTextTheme: TextTheme(
          headline: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 48.0,
            color: Colors.white
          ),
          button: TextStyle(
            fontSize: 18.0,
            color: Colors.white
          )
        )
      ),
      home: new MyHomePage(title: 'Name Game'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  void startCounter() {
    
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  Landing()
    );
  }
}
