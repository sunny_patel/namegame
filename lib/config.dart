class Config {
  static final Config _config = new Config._internal();

  factory Config() {
    return _config;
  }

  static final String beep2SoundUrl
    = 'https://firebasestorage.googleapis.com/v0/b/namegame-2.appspot.com/o/app%2Fpublic%2Fbeep2.mp3?alt=media&token=f2cf71c6-6d07-4673-8855-9cd6e1f77e23';
  
  static final String buzzerSoundUrl
    = 'https://firebasestorage.googleapis.com/v0/b/namegame-2.appspot.com/o/app%2Fpublic%2Fbuzzer.mp3?alt=media&token=18fcbdf6-32f3-4c05-8ef4-00392b8dae4b';
 
  static final String beep3SoundUrl
    = 'https://firebasestorage.googleapis.com/v0/b/namegame-2.appspot.com/o/app%2Fpublic%2Fbeep3.mp3?alt=media&token=85a3f8c5-069d-4027-971f-eb49da1b15a1';
  Config._internal();
}