import 'package:flutter/material.dart';

import './landingButton.dart';
import './decksGrid.dart';

class Landing extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 70.0,
        horizontal: 30.0
      ),
      color: Theme.of(context).primaryColor,
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Text('Name Games',
              style: Theme.of(context).primaryTextTheme.headline
            )
          ),
          Expanded(child: Container()),
          DecksGrid(),
          LandingButton(
            label: 'New Game',
            onPressed: ()=> {},
          ),
          Expanded(child: Container())
        ],
      )
    );
  }
} 