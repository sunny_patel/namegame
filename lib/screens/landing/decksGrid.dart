import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/deck.dart';
import '../../helpers/utils.dart';
import '../../widgets/newgame.dart';

class DecksGrid extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
  
    return new StreamBuilder(
      stream: Firestore.instance.collection('decks').snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData || snapshot.data.documents.length == 0)
          return Center(child: Text('Loading...'));

        return SizedBox(
          height: 500.0,
          width: 300.0,
          child: GridView.count(
                crossAxisCount: 2,
                children: snapshot.data.documents.map((DocumentSnapshot deck) {
                  return DeckStack(
                    deck: Deck(
                      name: Utils().capitalize(deck['name']),
                      cards: new List<NameGameCard>.from(
                        deck['cards'].map((card){
                          return NameGameCard(
                            name: Utils().capitalize(card)
                          );
                        }).toList()
                      )
                    )
                  );
                }).toList()
              )
        );
      }
    );
  }
} 

class DeckStack extends StatelessWidget {

  Deck deck;

  DeckStack({this.deck});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: RaisedButton(
        color: Colors.white,
        child: Center(
          child: Text(
            deck.name,
            style: TextStyle(
              fontSize: 18.0
            ),
          )
        ),
        elevation: 10.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => Container(
                child:  NewGame(
                  deck: deck,
                )
              )
            ),
          );
        },
      )
    );
  }
}