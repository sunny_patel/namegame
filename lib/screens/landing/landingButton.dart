import 'package:flutter/material.dart';

class LandingButton extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;

  LandingButton({
    @required this.label,
    @required this.onPressed
  });

  @override
  Widget build(BuildContext context) {

    return RaisedButton(
      onPressed: this.onPressed,
      color: Theme.of(context).primaryColorDark,
      child: Text(label, style: Theme.of(context).primaryTextTheme.button),
      padding: EdgeInsets.symmetric(
        vertical: 20.0,
        horizontal: 40.0
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(100.0))
      ),
    );
  }
}