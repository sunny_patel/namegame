class Deck {
  String name;
  String imgUrl;
  List<NameGameCard> cards;

  Deck({this.name, this.imgUrl, this.cards});
}

class NameGameCard {
  String name;

  NameGameCard({this.name});
}