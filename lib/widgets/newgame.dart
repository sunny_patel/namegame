import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:audioplayer/audioplayer.dart';

import '../models/deck.dart';
import '../helpers/gameClock.dart';
import '../config.dart';

class NewGame extends StatefulWidget {

  final Deck deck;

  NewGame({this.deck}) : super();

  @override
  State<StatefulWidget> createState() =>  _NewGameState(deck);
}

class _NewGameState extends State<NewGame> {

  final Deck deck;
  GameClock _gameClock;
  final gameTimeSeconds = 30;
  double currentGameTime;
  StreamSubscription gameTimerStream; 

  List<Team> teams = List<Team>.from([
    Team(name: 'penguins', icon: Icon(Icons.satellite), score: 0),
    Team(name: 'seahawks', icon: Icon(Icons.rowing), score: 0)
  ]);

  _NewGameState(this.deck) {
    this._gameClock = new GameClock(
      totalGameTimeInSeconds: this.gameTimeSeconds,
      onGameOver: () {
        this.onGameOver();
      }
    );
  }

  @override
  initState() {
    super.initState();
    this._gameClock.start();
  }

  @override
  dispose() {
    print('disosing of new game');
    this._gameClock.stop();
    super.dispose();
  }

  onGameOver() {

  }

  _cardsSwiper() {
    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return Card(
          elevation: 4.0,
          child: Center(
            child: Text(
              deck.cards[index].name,
              style: TextStyle(
                fontSize: 28.0
              )
            )
          ),
          shape: RoundedRectangleBorder(
            borderRadius:BorderRadius.circular(30.0)
          )
        );
      },
      itemCount: deck.cards.length,
      itemWidth: 350.0,
      itemHeight: 350.0,
      layout: SwiperLayout.TINDER,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(deck.name)
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: Column(
          children: [
            Expanded(child: Container()),
             _cardsSwiper(),
            Expanded(child: Container()),
            Container(
              child: Row(
                children: this.teams.map((team) {
                  return Expanded(child: FlatButton(
                    onPressed: () {
                      team.increaseScore();
                      this.setState(() {});
                    },
                    padding: EdgeInsets.only(
                      top: 20.0, bottom: 35.0
                    ),
                    child: Column(
                      children: <Widget>[
                        team.icon,
                        Text(team.name),
                        Text(team.score.toString())
                      ],
                    )
                  ));
                }).toList(),
              )
            )
          ]
        )
      ),
    );
  }
}

class Team {
  String name;
  Icon icon;
  int score = 1;

  Team({this.name, this.icon, this.score = 0}) : super();

  increaseScore() {
    this.score++;
  }
}