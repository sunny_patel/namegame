import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class CardList extends StatelessWidget {

  _swiper(deck) {
    return Swiper(
      itemBuilder: (BuildContext context,int index) {
        var name = deck[index]['name'].toString();
        return Padding(
          padding: EdgeInsets.all(50.0),
          child: Card(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))
            ),
            child: Center(
              child: Text(
                name.toUpperCase(),
                textScaleFactor: 2.5
              )
            )
          )
        );
      },
      itemCount: deck.length,
      control: new SwiperControl(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new StreamBuilder(
      stream: Firestore.instance.document('decks/brand').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData || !snapshot.data.exists)
          return Center(child: Text('Loading...'));
        
        var deck = snapshot.data.data['cards'];

        if (deck != null) {
          return Container(
            child: _swiper(deck)
          );
        }
      }
    );
  }
} 