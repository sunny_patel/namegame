import 'package:flutter/material.dart';

import './card.list.dart';

class Game extends StatefulWidget {

  @override
  GameState createState() => new GameState();
}

class GameState extends State<Game> {
  String deck;

  @override
  void initState() {

    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(deck),
      ),
      body: Container(
        padding: EdgeInsets.only(
          left: 15.0, right: 15.0,
          top: 100.0, bottom: 10.0
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: CardList(),
            ),
            Expanded(
              child: Container()
            ),
            Row(children: <Widget>[
              Expanded(
                child: RaisedButton(
                  child: Text('Next', textScaleFactor: 1.5,),
                  padding: EdgeInsets.only(
                    left: 35.0, right: 35.0,
                    top: 15.0, bottom: 15.0
                  ),
                  color: Theme.of(context).accentColor,
                  splashColor: Theme.of(context).hintColor,
                  onPressed: () {
                  },
                )
              )
            ])
          ],
        )
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).accentColor,
            icon: Icon(Icons.casino),
            title: Column(children: [
              Text('Team 1'),
              Text('5')
            ])
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pool),
            title: Column(children: [
              Text('Team 2'),
              Text('3')
            ])
          )
        ]
      )
    );
  }
}